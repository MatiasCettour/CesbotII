#include <iostream>
#include "../i2c/i2c.h"

using namespace std;

int main(int argc, char const *argv[])
{
if (argc < 2) 
{
	cout << "falta direccion i2c" << endl;
return -1;
}

	I2C i2c(std::stoi(argv[1]),1);

	bool fin = false; int comando = 0;
	while (!fin)
	{
		cin >> comando;
		if (comando == -1) fin=true;
		else
		{
			cout << i2c.escribir(comando) << endl;
		}
	}
return 0;
}
