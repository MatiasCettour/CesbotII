#include "i2c.h"

I2C::I2C(int _addr, int _nro_adaptador)
	: m_addr(_addr), m_nro_adaptador(_nro_adaptador)
{
  char filename[20];

  snprintf(filename, 19, "/dev/i2c-%d", m_nro_adaptador);
  m_file = open(filename, O_RDWR);

	if (m_file < 0) 
	{
		std::cout << "Error al abrir archivo i2c"<< std::endl;
		m_fallo = true;
	}

	else if (ioctl(m_file, I2C_SLAVE, m_addr) < 0) 
	{
		std::cout << "Error al buscar direccion"<< std::endl;
		m_fallo = true;
	}
}

__s32 I2C::leer()
{
	return i2c_smbus_read_byte(m_file);
}

int I2C::escribir(int _valor)
{
	return i2c_smbus_write_byte(m_file, _valor);
}
