#ifndef I2C_H
#define I2C_H

extern "C"
{
#include <linux/i2c.h>
#include <linux/i2c-dev.h>
#include <i2c/smbus.h>
}
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <iostream>

class I2C
{
public:
	I2C(int _addr, int _nro_adaptador);
	
	__s32 leer();
	int escribir(int _valor);
	
private:
	int m_addr, m_nro_adaptador, m_file;
	bool m_fallo = false;
};

#endif
