#ifndef ROBOT_H
#define ROBOT_H

#include "../i2c/i2c.h"

class Robot
{
public:
	Robot();

	int avanzar();
	int retroceder();
	int girar_derecha();
	int girar_izquierda();
	int detener_motores();

	~Robot();

private:
	I2C *m_i2c;
};

#endif

