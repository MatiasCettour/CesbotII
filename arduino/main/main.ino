#include <Wire.h>

const int pin1_motor1 = 20;
const int pin2_motor1 = 21;
const int pin1_motor2 = 22;
const int pin2_motor2 = 23;
const int pin1_motor3 = 24;
const int pin2_motor3 = 25;
const int pin1_motor4 = 26;
const int pin2_motor4 = 27;

void setup() 
{
	pinMode(pin1_motor1, OUTPUT);
	pinMode(pin2_motor1, OUTPUT);
	pinMode(pin1_motor2, OUTPUT);
	pinMode(pin1_motor2, OUTPUT);
	pinMode(pin1_motor3, OUTPUT);
	pinMode(pin1_motor3, OUTPUT);
	pinMode(pin1_motor4, OUTPUT);
	pinMode(pin1_motor4, OUTPUT);

 	Wire.begin(8);  
	Wire.onRequest(requestEvent);
}

void loop() 
{
  

}

void requestEvent() 
{
	Wire.write("hello "); 
}

void avanzar()
{
	digitalWrite(pin1_motor1, HIGH);
	digitalWrite(pin2_motor1, LOW);
	digitalWrite(pin1_motor2, HIGH);
	digitalWrite(pin2_motor2, LOW);
	digitalWrite(pin1_motor3, HIGH);
	digitalWrite(pin2_motor3, LOW);
	digitalWrite(pin1_motor4, HIGH);
	digitalWrite(pin2_motor4, LOW);
}
void retroceder()
{
	digitalWrite(pin1_motor1, LOW);
	digitalWrite(pin2_motor1, HIGH);
	digitalWrite(pin1_motor2, LOW);
	digitalWrite(pin2_motor2, HIGH);
	digitalWrite(pin1_motor3, LOW);
	digitalWrite(pin2_motor3, HIGH);
	digitalWrite(pin1_motor4, LOW);
	digitalWrite(pin2_motor4, HIGH);
}
void rotar_derecha()
{
	digitalWrite(pin1_motor1, HIGH);
	digitalWrite(pin2_motor1, LOW);
	digitalWrite(pin1_motor2, HIGH);
	digitalWrite(pin2_motor2, LOW);
	digitalWrite(pin1_motor3, HIGH);
	digitalWrite(pin2_motor3, LOW);
	digitalWrite(pin1_motor4, HIGH);
	digitalWrite(pin2_motor4, LOW);
}
void rotar_izquierda()
{
	digitalWrite(pin1_motor1, HIGH);
	digitalWrite(pin2_motor1, LOW);
	digitalWrite(pin1_motor2, HIGH);
	digitalWrite(pin2_motor2, LOW);
	digitalWrite(pin1_motor3, HIGH);
	digitalWrite(pin2_motor3, LOW);
	digitalWrite(pin1_motor4, HIGH);
	digitalWrite(pin2_motor4, LOW);
}