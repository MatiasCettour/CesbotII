#include "Robot.h"

Robot::Robot()
{	
	m_i2c = new i2c(8,1);
}

Robot::int avanzar()
{
	m_i2c->escribir(1);
}
Robot::int retroceder()
{
	m_i2c->escribir(2);
}
Robot::int girar_derecha()
{
	m_i2c->escribir(3);
}
Robot::int girar_izquierda()
{
	m_i2c->escribir(4);
}
Robot::int detener_motores()
{
	m_i2c->escribir(0);
}

Robot::~Robot()
{	
	delete m_i2c;
}